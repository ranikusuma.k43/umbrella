import React from "react";
import Input from "@material-ui/core/Input";
import ImageUploader from "react-images-upload";
import "./styleadddriver.css";

class AddDriver extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      age: "",
      relationship: "",
      gender: ""
    };
    this.changeHandler = this.changeHandler.bind(this);
  }
  changeHandler = e => {
    console.log(e.target.value)
    return this.setState({ username: e.target.value });
  }

  render() {
    return (
      <div>
        <h1>Enter the details</h1>
        <ImageUploader
          withIcon={true}
          buttonText="Choose images"
          imgExtension={[".jpg", ".gif", ".png", ".gif"]}
          maxFileSize={5242880}
        />
        <div>
          <Input placeholder="Name"></Input>
        </div>
        <div>
          <Input placeholder="Age"></Input>
        </div>
        <div>
          <Input placeholder="Relationship"></Input>
        </div>
        <div>
          <Input placeholder="Gender"></Input>
        </div>
        <button className="add-driver">Add driver</button>
      </div>
    );
  }
}

export default AddDriver;
