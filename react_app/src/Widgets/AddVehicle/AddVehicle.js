import React from "react";
import Input from "@material-ui/core/Input";
import ImageUploader from "react-images-upload";
import "./styleaddvehicle.css";

class AddVehicle extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      age: "",
      relationship: "",
      gender: ""
    };
    this.changeHandler = this.changeHandler.bind(this);
  }
  changeHandler = e => this.setState({ username: e.target.value });

  render() {
    return (
      <div>
        <h1>Enter the details</h1>
        <ImageUploader
          withIcon={true}
          buttonText="Choose images"
          imgExtension={[".jpg", ".gif", ".png", ".gif"]}
          maxFileSize={5242880}
        />
        <div><Input placeholder="Vehicle Identification Number(VIN)"></Input></div>
        <div><Input placeholder="Year"></Input></div>
        <div><Input placeholder="Make"></Input></div>
        <div><Input placeholder="Model"></Input></div>
        <button className="add-vehicle">Add</button>
      </div>
    );
  }
}

export default AddVehicle;
