import React from 'react';
import { Link } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import Btn from '../../SharedJSX/Inputs/Button/Buttons';
import "./stylehome.css"

const styles = {
  logo:
  {
    display: 'flex',
  },
  tag1:
  {
    display: 'flex',
    color: 'white',
    fontSize: 30,
  },
};
class Home extends React.Component {
  constructor(props) {
    super(props)
    this.state = { username: "" }
  }

  render() {

    return (
      <Grid container spacing={1} className="App">
        <Grid style={styles.logo} item xs={9} sm={9} direction="column-reverse" alignItems="flex-end">
          <img src="https://content.usaa.com/mcontent/static_assets/Media/globalHeader-usaaLogo-2016.svg" alt="Usaa logo"/><br />
        </Grid>

        <Grid style={styles.tag1} item xs={10} sm={10} direction="column" alignItems="flex-end">
          <p>Hello,<br/>
            <b>Please select the Quote</b></p>
        </Grid>

        <Grid style={styles.button} xs={11} sm={11} container direction="column" alignItems="center" alignContent="flex-end">

          <Link style={{ textDecoration: 'none' }} to='/quoteresults'><Btn id="button1" variant="contained">Your TX Quote</Btn></Link>

         
           <p>(OR)</p> 

          <Link style={{ textDecoration: 'none' }}><Btn id="button2" variant="outlined">Get a New Quote</Btn></Link>

        </Grid>

      </Grid>
    );
  }
}


export default Home;
