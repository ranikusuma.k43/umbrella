import React from "react";
import Input from "@material-ui/core/Input";
import Select from "@material-ui/core/Select";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from '@material-ui/core/MenuItem';
import ImageUploader from "react-images-upload";
import "./styleaddproperty.css";
import {addProperty, resetInfo} from '../../actions'
import { connect } from 'react-redux'
import history from '../../utils/history';

class AddProperty extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      propertyData: {
      location: "",
      apartment: "",
      city: "",
      state : "",
      zipcode: ""
      }
    };
    this.changeHandler = this.changeHandler.bind(this);
  }
  changeHandler = e => {
    const name = e.target.name;
    let {propertyData} = this.state;
    propertyData[name] = e.target.value;
    this.setState({ propertyData });
  }
  componentWillUnmount() {
    this.props.resetInfo();
  }
  addPropertyData() {
    
    this.props.addProperty(this.state.propertyData).then( () => {
      this.props.addSuccessData &&  history.push('/quoteresults');
    });
    
  }
  render() {
    const cityLabel = this.state.propertyData.city ? "removeCityLabel" : "addCityLabel";
    const stateLabel = this.state.propertyData.state ? "removeStateLabel" : "addStateLabel";
    const propertyData = this.props.isPropertyEdited && this.props.propertyInfo ? this.props.propertyInfo[0] : this.state.propertyData;
    const headerTitle = this.props.isPropertyEdited ? 'Property Details' : 'Add New Property';
    const buttonLabel = this.props.isPropertyEdited ? 'Update' : 'Add';
    return (
      <div>
        <h1>{headerTitle}</h1>
        { !this.props.isPropertyEdited &&   <ImageUploader
          withIcon={true}
          buttonText="Choose images"
          imgExtension={[".jpg", ".gif", ".png", ".gif"]}
          maxFileSize={5242880} 
        /> }
        { this.props.isPropertyEdited &&   <img className="addPropertyImage" alt="" /> }
        <div><Input name="location" placeholder="Location" value={propertyData.location} onChange={this.changeHandler}></Input></div>
        <div><Input name="apartment" placeholder="Suit/Apartments" value={propertyData.apartment} onChange={this.changeHandler}></Input></div>
        <div>
        <InputLabel id="age-label"  className={cityLabel}>City</InputLabel>
          <Select  name="city" placeholder="City" value={propertyData.city} onChange={this.changeHandler} className="cityStyle">
          <MenuItem value="New York">New York</MenuItem>
          <MenuItem value="Chicago">Chicago</MenuItem>
          <MenuItem value="Austin">Austin</MenuItem>
          <MenuItem value="Charlotte">Charlotte</MenuItem>
          <MenuItem value="San Antonio">San Antonio</MenuItem>
          </Select>
          </div>
        <div>
        <InputLabel id="age-label" className={stateLabel}>State</InputLabel>
          <Select  name="state" placeholder="state" value={propertyData.state} onChange={this.changeHandler} className="cityStyle">
          <MenuItem value="Texas">Texas</MenuItem>
          <MenuItem value="Texas">Texas</MenuItem>
          <MenuItem value=" New York"> New York</MenuItem>
          <MenuItem value="San Antonio">San Antonio</MenuItem>
          </Select>
          </div>
        <div><Input name="zipcode" placeholder="ZipCode" value={propertyData.zipcode} onChange={this.changeHandler}></Input></div>
        <button className="add-property" onClick={()=> this.addPropertyData()}>{buttonLabel}</button>
      </div>
    );
  }
}

 
const mapStateToProps=(state)=>{
  const {propertyInfo,isPropertyEdited,addSuccessData} = state.properties;
  return {
    propertyInfo, isPropertyEdited, addSuccessData
  }
}
export default connect(mapStateToProps,{addProperty,resetInfo})(AddProperty)