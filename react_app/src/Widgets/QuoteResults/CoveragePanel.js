import React from 'react'


class CoveragePanel extends React.Component{
    render(){
        return(
             
                <div className="form-group">   
                    <div className="quoteresultstext">Protects you, your household members & properties</div>
                     <div className="cov-card">    
                        <table align="left" width="100%">
                         <tbody>
                            <col width="70%" />
                            <col width="20%" />
                            <button className="coverageselection">
                                <tr>
                                <td><tr >$100,000/$300,000 </tr><tr>(per person/per accident)</tr></td>
                                <td styles={{marginLeft:"100px"}}>$17.85</td>
                                </tr>
                            </button><br />
                            <button className="coverageselection">
                                <tr>
                                <td><tr>$300,000/$500,000</tr><tr>(per person/per accident)</tr></td>
                                <td>$21.52</td>
                                </tr></button><br />
                            <button className="coverageselection"><tr>
                                <td> <tr>$500,000/$1000,000</tr> <tr>(per person/per accident)</tr></td>
                                <td>$32.25</td>
                                </tr></button><br />
                            <button className="coverageselection"><tr>
                                <td><tr>$1000,000/$2000,00</tr> <tr> (per person/per accident) </tr></td>
                                <td>$65.88</td>
                                </tr></button>
                          </tbody>
                        </table>
                     </div>
            </div>
        )
    }
}
export default CoveragePanel