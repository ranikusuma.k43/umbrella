import React from 'react'
import {fetchDrivers} from '../../actions'
import { connect } from 'react-redux'
import './stylequoteresults.css'
class DisplayDriver extends React.Component{

    componentDidMount(){
        this.props.fetchDrivers();
    }

    driverdetails=(driver)=>{
        return driver.name
    }


    render(){
        console.log(this.props.drivers)
        console.log(this.props.drivers.length)
        return(
           <div className="driverinfo">
               {this.props.drivers.map(driver=>{
                   return(<span >
                   <div>Driver Name:{driver.Name}</div>
                   <div>Driver Gender:{driver.Gender}</div>
               </span>)
               })}
           </div>
        )
    }
}

const mapStateToProps=(state)=>{
    return {
        "drivers":state.drivers
    }
}
export default connect(mapStateToProps,{fetchDrivers})(DisplayDriver)