import React from 'react'
import history from '../../utils/history'

import {fetchProperties, getPropertyDetails} from '../../actions'
import { connect } from 'react-redux'
import './stylequoteresults.css'

class DisplayProperties extends React.Component{

    componentDidMount(){
        this.props.fetchProperties();
    }
    editProperty(index) {
        this.props.getPropertyDetails();
        history.push('/addproperty')
    }
    render(){
        return(
           <div className="propertyInfo">
               {this.props.propertyAPIData && this.props.propertyAPIData.map((property, index)=>{
                   return(<div key={index} className="propertiesStyle" onClick={() =>this.editProperty(index)}>
                   {property.location}, {property.apartment}
                   {property.city} {property.state} {property.zipcode}
               </div>)
               })}
           </div>
        )  
    }
}

const mapStateToProps=(state)=>{
    const { propertyAPIData } = state.properties;
    return {
        propertyAPIData
    }
}
export default connect(mapStateToProps,{fetchProperties, getPropertyDetails})(DisplayProperties)