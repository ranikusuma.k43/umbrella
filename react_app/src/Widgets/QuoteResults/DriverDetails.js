import React from 'react'
import history from '../../utils/history'
import DisplayDriver from './DisplayDriver'
import './stylequoteresults.css'


class DriverDetails extends React.Component{

    onAddDriverClick=()=>{
        history.push('/adddriver')
    }
    render(){
        return(
            <div>
                <div className="drivers">
                    <span className="drivertext"><b>Drivers</b></span>
                    <button className="driveradd" onClick={this.onAddDriverClick}>Add+</button>
                </div>
                <DisplayDriver />
            </div>
        )
    }
}
export default DriverDetails