import React from 'react'
import history from '../../utils/history'
import DisplayProperties from './DisplayProperties'
import './stylequoteresults.css'
import { resetInfo } from '../../actions'
import { connect } from 'react-redux'

class PropertyDetails extends React.Component{

    AddPropertyClick=()=>{
        this.props.resetInfo().then( () => {
            history.push('/addproperty')
        });
        
    }
    render(){
        return(
            <div>
                <div className="drivers">
                    <span className="drivertext"><b>Properties</b></span>
                    <button className="driveradd" onClick={this.AddPropertyClick}>Add+</button>
                    <DisplayProperties />
                </div>
               
            </div>
        )
    }
}
const mapStateToProps=(state)=>{
    return state;
}
 
export default connect(mapStateToProps,{resetInfo})(PropertyDetails)