import React from 'react'
import DriverDetails from '../QuoteResults/DriverDetails'
import Header from "./Header";
import CoveragePanel from './CoveragePanel'
import './stylequoteresults.css'
import VehicleDetails from './VehicleDetails';
import PropertyDetails from './PropertyDetails';

class QuoteResultsPage extends React.Component{
    render(){
            return(
                <div>
                    <Header title="TX Umbrella Insurance Quote" />
                    <CoveragePanel />
                    <DriverDetails />
                    <VehicleDetails />
                    <PropertyDetails />
                </div>
            )
    }
}
export default QuoteResultsPage