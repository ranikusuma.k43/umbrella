import React from 'react'
import history from '../../utils/history'
import DisplayVehicle from './DisplayVehicle'
import './stylequoteresults.css'

class VehicleDetails extends React.Component{

    onAddDriverClick=()=>{
        history.push('/addvehicle')
    }
    render(){
        return(
            <div>
                <div className="vehicles">
                    <span className="vehicletext"><b>Vehicles</b></span>
                    <button className="vehicleadd" onClick={this.onAddDriverClick}>Add+</button>
                </div>
                <DisplayVehicle />
            </div>
        )
    }
}
export default VehicleDetails