import dbjsonapi from '../apis/dbjsonapi'


export const adddriveraction=(driver)=>async dispatch =>{
    const response = await dbjsonapi.post("/drivers", {...driver});
    console.log('chaitu'+response.data);
    dispatch({
        type:"DRIVERUPDATE",
        payload:response.data
    })
}

export const fetchDrivers=()=>async dispatch => {
    const response = await dbjsonapi.get("/drivers");
    console.log(response);
    dispatch({
        type:"FETCHDRIVERS",
        payload: response.data
    })
}

export const fetchVehicles=()=>async dispatch =>{
    const response = await dbjsonapi.get("/vehicles");
    console.log(response);
    dispatch({
        type:"FETCHVEHICLES",
        payload: response.data
    })
}


export const fetchProperties=()=>async dispatch =>{
    const response = await dbjsonapi.get("/properties");
    console.log("Property Fetch",response);
    dispatch({
        type:"FETCHPROPERTIES",
        payload: response.data
    })
}


export const addProperty=()=>async dispatch =>{
    const response = await dbjsonapi.get("/addProperty");
    console.log("Add Property", response);
    dispatch({
        type:"ADDPROPERTY",
        payload: response.data
    })
}
export const updateProperty=()=>async dispatch =>{
    const response = await dbjsonapi.get("/updateProperty");
    console.log("Update Property", response);
    dispatch({
        type:"UPDATEPROPERTY",
        payload: response.data
    })
}

export const getPropertyDetails=()=>async dispatch =>{
    const response = await dbjsonapi.get("/getPropertyDetails");
    console.log("GET Property", response);
    dispatch({
        type:"GETPROPERTYDETAILs",
        payload: response.data
    })
}

export const resetInfo=()=>async dispatch =>{
    dispatch({
        type:"RESETINFO",
    })
}