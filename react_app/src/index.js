import React from 'react';
import ReactDOM from 'react-dom';
import Home from './Widgets/Home/Home';
import Login from './Widgets/Login/Login';
import * as serviceWorker from './serviceWorker';
import QuoteResultsPage from './Widgets/QuoteResults/QuoteResultsPage'
import {Router,Route} from 'react-router-dom' 
import history from './utils/history'
import { Provider } from 'react-redux'
import {createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers'
import AddDriver from './Widgets/AddDriver/AddDriver'
import AddVehicle from './Widgets/AddVehicle/AddVehicle'
import AddProperty from './Widgets/Property/AddProperty'
import './index.css'

const enhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose
const store = createStore(reducers, enhancer(applyMiddleware(thunk)))


class App extends React.Component{
    render(){
        return(
            <Router history={history}>
                <div>
                     <Route exact path="/" component={Home} />
                     <Route path="/quoteresults" exact component={QuoteResultsPage} />
                     <Route path="/adddriver" exact component={AddDriver} />
                     <Route path="/addvehicle" component={AddVehicle} />
                     <Route path="/addproperty" component={AddProperty} />
                                          <Route path="/addproperty" component={AddProperty} />



                </div>
            </Router>
        )
    }
}


ReactDOM.render( 
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
 );

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
