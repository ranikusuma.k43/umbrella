import { combineReducers } from 'redux'
import driverreducer from './driverreducer';
import vehiclereducer from './vehiclereducer';
import propertyreducer from './propertyreducer';

export default combineReducers({
    "drivers":driverreducer,
    "vehicles": vehiclereducer,
    "properties": propertyreducer
})