export const INITIAL_STATE = {
    propertyAPIData: {},
    addSuccessData: {},
    updateSuccessData: {},
    propertyInfo: {},
    isPropertyEdited: false
}
export default (state=[], action)=>{
    console.log('Inside Property Reducer'+action.payload)
    switch(action.type){
        case "ADDPROPERTY":  {
            return {...state, addSuccessData:action.payload}
        }
        case "UPDATEPROPERTY": {
            return {...state, updateSuccessData:action.payload}
        }
        case "FETCHPROPERTIES": {
            return {...state, propertyAPIData:action.payload}
        }
        case "GETPROPERTYDETAILs" : {
            return {...state, propertyInfo:action.payload, isPropertyEdited: true}
        }
        case "RESETINFO" : {
            return {...state, isPropertyEdited: false}
        }
        default:return state
    }
}